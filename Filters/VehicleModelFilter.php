<?php

namespace Modules\VehicleCommon\Filters;

use EloquentFilter\ModelFilter;
use Illuminate\Contracts\Database\Eloquent\Builder;

class VehicleModelFilter extends ModelFilter
{
    protected $drop_id = false;

    /**
     * Apply filter with search param on title and description.
     *
     * @param string $search
     * @return Builder
     */
    public function search($search)
    {
        return $this->where(function ($q) use ($search) {
            $q->where('title', 'LIKE', '%' . $search . '%')
                ->orWhere('description', 'LIKE', '%' . $search . '%');
        });
    }

    /**
     * Apply sort on model.
     *
     * @param string $sort
     * @return void
     */
    public function sortBy($sort)
    {
        $sortDdata = explode('|', $sort);
        $column = 'id';
        $direction = 'desc';

        if (count($sortDdata) > 1) {
            $column = $sortDdata[0];
            $direction = $sortDdata[1];
        }

        return $this->orderBy($column, $direction);
    }

    /**
     * Eager load relations
     *
     * @param string $relations
     * @return void
     */
    public function relation($relations)
    {
        return $this->with(explode(',', $relations));
    }

    public function makeIds($makeIds)
    {
        return $this->whereIn('make_id', explode(',', $makeIds));
    }
}
