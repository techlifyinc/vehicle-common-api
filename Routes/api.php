<?php

use Illuminate\Support\Facades\Route;
use Modules\VehicleCommon\Http\Controllers\VehicleMakeController;
use Modules\VehicleCommon\Http\Controllers\VehicleModelController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Vehicle make routes
Route::get(
    'vehicle-makes',
    [VehicleMakeController::class, 'index']
)->middleware("TechlifyAccessControl:vehicle_common_vehicle_make.read")
    ->middleware('TechlifyAccessControl:state:logged-in');

Route::post(
    'vehicle-makes',
    [VehicleMakeController::class, 'store']
)->middleware('TechlifyAccessControl:vehicle_common_vehicle_make.create')
    ->middleware('TechlifyAccessControl:state:logged-in');

Route::put(
    'vehicle-makes/{vehicleMake}',
    [VehicleMakeController::class, 'update']
)->middleware('TechlifyAccessControl:vehicle_common_vehicle_make.update')
    ->middleware('TechlifyAccessControl:state:logged-in');

Route::delete(
    'vehicle-makes/{vehicleMake}',
    [VehicleMakeController::class, 'delete']
)->middleware('TechlifyAccessControl:vehicle_common_vehicle_make.delete')
    ->middleware('TechlifyAccessControl:state:logged-in');


// Vehicle model routes
Route::get(
    'vehicle-models',
    [VehicleModelController::class, 'index']
)->middleware('TechlifyAccessControl:vehicle_common_vehicle_model.read')
    ->middleware('TechlifyAccessControl:state:logged-in');

Route::post(
    'vehicle-models',
    [VehicleModelController::class, 'store']
)->middleware('TechlifyAccessControl:vehicle_common_vehicle_model.create')
    ->middleware('TechlifyAccessControl:state:logged-in');

Route::put(
    'vehicle-models/{vehicleModel}',
    [VehicleModelController::class, 'update']
)->middleware('TechlifyAccessControl:vehicle_common_vehicle_model.update')
    ->middleware('TechlifyAccessControl:state:logged-in');

Route::delete(
    'vehicle-models/{vehicleModel}',
    [VehicleModelController::class, 'delete']
)->middleware('TechlifyAccessControl:vehicle_common_vehicle_model.delete')
    ->middleware('TechlifyAccessControl:state:logged-in');
