<?php

namespace Modules\VehicleCommon\Traits;

use Modules\VehicleCommon\Entities\VehicleMake;

trait VehicleMakeAttribute
{
    
    /**
     * Get the logo url attribute
     *
     * @return string
     */
    public function getLogoUrlAttribute()
    {
        return $this->getFirstMediaUrl(VehicleMake::MEDIA_COLLECTION_NAME);
    }
}
