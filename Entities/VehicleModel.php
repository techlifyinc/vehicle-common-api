<?php

namespace Modules\VehicleCommon\Entities;

use EloquentFilter\Filterable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\LaravelCore\Entities\LoggableModel;
use Modules\LaravelCore\Entities\TechlifyModel;
use Modules\VehicleCommon\Filters\VehicleModelFilter;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;

class VehicleModel extends TechlifyModel implements HasMedia
{
    const MEDIA_COLLECTION_NAME = 'vehicle-models';

    protected $appends = [
        'logo_url',
    ];

    use LoggableModel;
    use InteractsWithMedia;
    use SoftDeletes;
    use Filterable;

    /**
     * Apply filter class
     *
     * @return void
     */
    public function modelFilter()
    {
        return $this->provideFilter(VehicleModelFilter::class);
    }

    /**
     * Get the logo url attribute
     *
     * @return string
     */
    public function getLogoUrlAttribute()
    {
        return $this->getFirstMediaUrl(VehicleModel::MEDIA_COLLECTION_NAME);
    }

    public function make()
    {
        return $this->hasOne(VehicleMake::class, 'id', 'make_id');
    }
}
