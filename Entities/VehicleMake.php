<?php

namespace Modules\VehicleCommon\Entities;

use EloquentFilter\Filterable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\LaravelCore\Entities\LoggableModel;
use Modules\LaravelCore\Entities\TechlifyModel;
use Modules\VehicleCommon\Filters\VehicleMakeFilter;
use Modules\VehicleCommon\Traits\VehicleMakeAttribute;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;

class VehicleMake extends TechlifyModel implements HasMedia
{
    const MEDIA_COLLECTION_NAME = 'vehicle-makes';

    protected $appends = [
        'logo_url',
    ];

    use LoggableModel;
    use InteractsWithMedia;
    use VehicleMakeAttribute;
    use SoftDeletes;
    use Filterable;

    /**
     * Apply filter class
     *
     * @return void
     */
    public function modelFilter()
    {
        return $this->provideFilter(VehicleMakeFilter::class);
    }

    public function models()
    {
        return $this->hasMany(VehicleModel::class, 'make_id', 'id');
    }
}
