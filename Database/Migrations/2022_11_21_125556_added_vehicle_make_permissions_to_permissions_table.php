<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class AddedVehicleMakePermissionsToPermissionsTable extends Migration
{
    private $permissionModels = [
        ['slug' => 'vehicle_common_vehicle_make.create', 'label' => "Vehicle Make: Create"],
        ['slug' => 'vehicle_common_vehicle_make.update', 'label' => "Vehicle Make: Update"],
        ['slug' => 'vehicle_common_vehicle_make.read', 'label' => "Vehicle Make: Read"],
        ['slug' => 'vehicle_common_vehicle_make.delete', 'label' => "Vehicle Make: Delete"],
    ];
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $permissions = DB::table('permissions')
            ->whereIn('slug', collect($this->permissionModels)
                ->pluck('slug'))
            ->get();

        if ($permissions) {
            DB::table('permissions')
                ->insert($this->permissionModels);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $slugs = collect($this->permissionModels)
            ->pluck('slug');

        DB::table('permissions')
            ->whereIn("slug", $slugs)
            ->delete();
    }
}
