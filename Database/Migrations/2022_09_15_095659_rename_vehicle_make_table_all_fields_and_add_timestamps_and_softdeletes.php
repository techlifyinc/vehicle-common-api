<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class RenameVehicleMakeTableAllFieldsAndAddTimestampsAndSoftdeletes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('vehicle_makes', function(Blueprint $table) {
            $table->renameColumn('mid', 'id')->change();
            $table->renameColumn('msid', 'status_id')->change();
            $table->renameColumn('make', 'title')->change();
            $table->renameColumn('uid', 'creator_id')->change();
            $table->renameColumn('createdTimestamp', 'created_at')->change();
            $table->timestamp('updated_at')->nullable();
            $table->unsignedBigInteger('client_id')->nullable();

            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('vehicle_makes', function(Blueprint $table) {
            $table->renameColumn('id', 'mid')->change();
            $table->renameColumn('status_id', 'msid')->change();
            $table->renameColumn('title', 'make')->change();
            $table->renameColumn('creator_id', 'uid')->change();
            $table->renameColumn( 'created_at', 'createdTimestamp')->change();
            $table->dropColumn('updated_at');
            $table->dropColumn('client_id');

            $table->dropSoftDeletes();
        });
    }
}
