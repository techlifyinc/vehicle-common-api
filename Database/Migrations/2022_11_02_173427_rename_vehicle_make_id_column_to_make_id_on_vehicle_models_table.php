<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RenameVehicleMakeIdColumnToMakeIdOnVehicleModelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('vehicle_models', function(Blueprint $table) {
            $table->renameColumn('vehicle_make_id','make_id')->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

        Schema::table('vehicle_models', function(Blueprint $table) {
            $table->renameColumn('make_id','vehicle_make_id')->change();
        });
    }
}
