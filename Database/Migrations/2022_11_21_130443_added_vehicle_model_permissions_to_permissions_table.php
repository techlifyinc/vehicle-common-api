<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class AddedVehicleModelPermissionsToPermissionsTable extends Migration
{
    private $permissionModels = [
        ['slug' => 'vehicle_common_vehicle_model.create', 'label' => "Vehicle Model: Create"],
        ['slug' => 'vehicle_common_vehicle_model.update', 'label' => "Vehicle Model: Update"],
        ['slug' => 'vehicle_common_vehicle_model.read', 'label' => "Vehicle Model: Read"],
        ['slug' => 'vehicle_common_vehicle_model.delete', 'label' => "Vehicle Model: Delete"],
    ];
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $permissions = DB::table('permissions')
            ->whereIn('slug', collect($this->permissionModels)
                ->pluck('slug'))
            ->get();

        if ($permissions) {
            DB::table('permissions')
                ->insert($this->permissionModels);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $slugs = collect($this->permissionModels)
            ->pluck('slug');

        DB::table('permissions')
            ->whereIn("slug", $slugs)
            ->delete();
    }
}
