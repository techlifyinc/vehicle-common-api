<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RenameVehicleModelsAllFieldsAndAddTimestampsAndSoftdeletes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('vehicle_models', function(Blueprint $table) {
            $table->renameColumn('mdid', 'id')->change();
            $table->renameColumn('mid', 'vehicle_make_id')->change();
            $table->renameColumn('model', 'title')->change();
            $table->unsignedBigInteger('client_id')->nullable();
            $table->unsignedBigInteger('creator_id')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('vehicle_models', function(Blueprint $table) {
            $table->renameColumn('id', 'mdid')->change();
            $table->renameColumn('vehicle_make_id', 'mid')->change();
            $table->renameColumn('title', 'model')->change();
            $table->dropColumn('client_id');
            $table->dropColumn('creator_id');
            $table->dropTimestamps();
            $table->dropSoftDeletes();
        });
    }
}
