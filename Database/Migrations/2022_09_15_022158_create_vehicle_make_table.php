<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVehicleMakeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('vehicle_make')) {
            Schema::create('vehicle_make', function (Blueprint $table) {
                $table->unsignedBigInteger('mid')->autoIncrement();
                $table->unsignedBigInteger('msid');
                $table->string('make')->unique();
                $table->text('description')->nullable();
                $table->unsignedBigInteger('uid')->nullable();
                $table->timestamp('createdTimestamp')->nullable();
                $table->string('logo')->nullable();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // we can not delete the existing table or data
    }
}
