<?php

namespace Modules\VehicleCommon\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Response;
use Modules\VehicleCommon\Entities\VehicleMake;
use Modules\VehicleCommon\Entities\VehicleModel;

class VehicleModelController extends Controller
{
    /**
     * Load vehicle models.
     *
     * @return Response
     */
    public function index()
    {
        $filters = request([
            'search',
            'sort_by',
            'relation',
        ]);

        $perPage = request('per_page', 25);

        $vehicleModels = VehicleModel::filter($filters)
            ->with('make')
            ->paginate($perPage);

        return response($vehicleModels, 200);
    }

    /**
     * Add a new vehicle model record.
     *
     * @return Response
     */
    public function store()
    {
        $this->validate(
            request(),
            [
                'title' => 'required|unique:vehicle_models,title',
                'make_id' => 'required|exists:' . (new VehicleMake())->getTable() . ',id',
            ]
        );

        $vehicleModel = new VehicleModel();
        $vehicleModel->title = request('title');
        $vehicleModel->make_id = request('make_id');
        $vehicleModel->description = request('description', '');
        $vehicleModel->creator_id = auth()->id();
        $vehicleModel->client_id = auth()->check() ? auth()->user()->client_id : null;
        $vehicleModel->save();

        // add logo to media library
        if (request()->hasFile('logo')) {
            $vehicleModel->addMediaFromRequest('logo')
                ->toMediaCollection(VehicleModel::MEDIA_COLLECTION_NAME);
        }

        return response(['vehicleModel' => $vehicleModel], Response::HTTP_OK);
    }

    /**
     * Update vehicle model record.
     *
     * @param VehicleModel $vehicleModel
     * @return Response
     */
    public function update(VehicleModel $vehicleModel)
    {
        $this->validate(
            request(),
            [
                'title' => 'required|unique:vehicle_makes,title,' . $vehicleModel->id,
                'make_id' => 'required|exists:' . (new VehicleMake())->getTable() . ',id',
            ]
        );

        $vehicleModel->title = request('title');
        $vehicleModel->make_id = request('make_id');
        $vehicleModel->description = request('description', '');
        $vehicleModel->save();

        // add logo to media library
        if (request()->hasFile('logo')) {
            // delete existing media
            $vehicleModel->clearMediaCollection(VehicleModel::MEDIA_COLLECTION_NAME);
            // upload new media
            $vehicleModel->addMediaFromRequest('logo')
                ->toMediaCollection(VehicleModel::MEDIA_COLLECTION_NAME);
        }

        return response(['item' => $vehicleModel], Response::HTTP_OK);
    }

    /**
     * Delete vehicle model record.
     *
     * @param VehicleModel $vehicleModel
     * @return Response
     */
    public function delete(VehicleModel $vehicleModel)
    {
        // clear all uploaded media.
        $vehicleModel->clearMediaCollection(VehicleModel::MEDIA_COLLECTION_NAME);
        // delete vehicle model.
        $vehicleModel->delete();

        return response(
            ['message' => 'Vehicle model deleted.'],
            Response::HTTP_OK
        );
    }
}
