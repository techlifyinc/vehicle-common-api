<?php

namespace Modules\VehicleCommon\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Response;
use Modules\VehicleCommon\Entities\VehicleMake;

class VehicleMakeController extends Controller
{
    /**
     * Load vehicle makes.
     *
     * @return Response
     */
    public function index()
    {
        $filters = request([
            'search',
            'sort_by',
            'relation',
        ]);

        $perPage = request('per_page', 25);

        $vehicleMakes = VehicleMake::filter($filters)
            ->with('models')
            ->paginate($perPage);

        return response($vehicleMakes, 200);
    }

    /**
     * Add a new vehicle make record.
     *
     * @return Response
     */
    public function store()
    {
        $this->validate(
            request(),
            [
                'title' => 'required|unique:vehicle_makes,title',
            ]
        );

        $vehicleMake = new VehicleMake();
        $vehicleMake->title = request('title');
        $vehicleMake->status_id = request('status_id', null);
        $vehicleMake->description = request('description', '');
        $vehicleMake->creator_id = auth()->id();
        $vehicleMake->client_id = auth()->check() ? auth()->user()->client_id : null;
        $vehicleMake->save();

        // add logo to media library
        if (request()->hasFile('logo')) {
            $vehicleMake->addMediaFromRequest('logo')
                ->toMediaCollection(VehicleMake::MEDIA_COLLECTION_NAME);
        }

        return response(['vehicleMake' => $vehicleMake], Response::HTTP_OK);
    }

    /**
     * Update vehicle make record.
     *
     * @param VehicleMake $vehicleMake
     * @return Response
     */
    public function update(VehicleMake $vehicleMake)
    {
        $this->validate(
            request(),
            [
                'title' => 'required|unique:vehicle_makes,title,' . $vehicleMake->id,
            ]
        );

        $vehicleMake->title = request('title');
        $vehicleMake->status_id = request('status_id', null);
        $vehicleMake->description = request('description', '');
        $vehicleMake->save();

        // add logo to media library
        if (request()->hasFile('logo')) {
            // delete existing media
            $vehicleMake->clearMediaCollection(VehicleMake::MEDIA_COLLECTION_NAME);
            // upload new media
            $vehicleMake->addMediaFromRequest('logo')
                ->toMediaCollection(VehicleMake::MEDIA_COLLECTION_NAME);
        }

        return response(['item' => $vehicleMake], Response::HTTP_OK);
    }

    /**
     * Delete vehicle make record.
     *
     * @param VehicleMake $vehicleMake
     * @return Response
     */
    public function delete(VehicleMake $vehicleMake)
    {
        // clear all uploaded media.
        $vehicleMake->clearMediaCollection(VehicleMake::MEDIA_COLLECTION_NAME);
        // delete vehicle make.
        $vehicleMake->delete();

        return response(
            ['message' => 'Vehicle make deleted.'],
            Response::HTTP_OK
        );
    }
}
